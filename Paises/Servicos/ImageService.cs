﻿
namespace Paises.Servicos
{
    using Svg;
    using System;
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.Drawing.Imaging;
    using System.IO;
    using System.Net;
    using System.Windows.Forms;

    public class ImageService
    {

        public byte[] SvgToByteArray(string link)
        {
            //salva o ficheiro svg localmente
            WebClient webClient = new WebClient();
            webClient.DownloadFile(link, @"images\localFileName");
            //byte[] imageBytes = webClient.DownloadData(paisB.Flag); //prob com svg

            //lê o svg local
            var svgDocument = SvgDocument.Open(@"images\localFileName");

            Bitmap flag = new Bitmap(200, 150);

            try
            {
                flag = svgDocument.Draw();
                
                //salva o fich convertido do svg para o tipo definido
                flag.Save(@"images\teste.png", ImageFormat.Png);
            }
            catch (Exception r)
            {
                MessageBox.Show(r.Message);
            }

            ImageConverter converter = new ImageConverter();

            return (byte[])converter.ConvertTo(flag, typeof(byte[]));
        }



        public Image byteArrayToSvg(byte[] byteArray)
        {
            //provisório, filler
            MemoryStream ms = new MemoryStream(byteArray);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }


        public void SvgToPng(string link)
        {

            WebClient webClient = new WebClient();
            webClient.DownloadFile(link, @"images\localFileName");

            var svgDocument = SvgDocument.Open(@"images\localFileName");

            Bitmap flag = new Bitmap(200, 150);

            flag.Save(@"images\teste.png", ImageFormat.Png);
 
        }


        public Bitmap ResizeImage(Image image, int width, int height)
        {
            var imgRect = new Rectangle(0, 0, width, height);
            var resultImage = new Bitmap(width, height);

            resultImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(resultImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, imgRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return resultImage;
        }



    }
}
