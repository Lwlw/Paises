﻿namespace Paises.Servicos
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Windows.Forms;

    public class MapService
    {
        double posY;

        public Point CoordsToMapa(double lng, double lat, int width, int height, PictureBox pictureBoxPin)
        {
            //Y
            int mapWidth = width;
            int mapHeight = height;
            double latRad = (lat * Math.PI) / 180;
            double mercN = Math.Log(Math.Tan((Math.PI / 4) + (latRad / 2)));
            double y = ((mapHeight / 2) - (mapWidth * mercN / (2 * Math.PI)));

            //X
            double lngDegree = mapWidth / 360.0;
            double x = (lng + 180) * lngDegree - pictureBoxPin.Width / 2;

            Point ponto = new Point((int)x, (int)y);

            return ponto;
        }


        public List<Double> MapaToCoords(int x, int y, int width, int height, PictureBox pictureBoxPin)
        {
            int mapWidth = width;
            int mapHeight = height;
            double mercN = ((mapHeight / 2) - y) * (2 * Math.PI) / mapWidth;
            double latRad = 2 * (Math.Atan(Math.Pow(Math.E, mercN)) - Math.PI / 4);
            double lat = latRad * 180 / Math.PI;

            double lngDegree = mapWidth / 360.0;
            double lng = x / lngDegree + pictureBoxPin.Width / 2 - 180;

            var coords = new List<Double> { lng, lat };

            return coords;
        }


        public int timeZoneLocX(string zoneTime, int width)
        {

            zoneTime = zoneTime.Remove(0, 3);
            string[] zT = zoneTime.Split(':');
            zoneTime = string.Join(".", zT);
            double tZ = Convert.ToDouble(zoneTime, System.Globalization.CultureInfo.InvariantCulture);

            double unit = width / 25;

            posY = tZ * unit + 11.5 * unit - 5;

            var exced = 11.5 * unit - 5 - tZ * unit;

            if ((posY - exced) % unit != 0)
                return (int)(posY - posY % unit + unit / 2) + 10;
            else
                return (int)posY;
        }


        public int timeZoneLocX2(decimal tZ, int width)
        {

            int unit = width / 25;

            posY = (int)tZ * unit + 12 * unit;

            //MessageBox.Show(unit.ToString());

            return (int)posY;
        }


    }
}
