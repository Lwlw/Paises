﻿namespace Paises.Modelos
{
    using System;
    using System.Collections.Generic;

    public class Country
    {

        public string Name { get; set; }  //mandatory
        public List<string> TopLevelDomain { get; set; }  //mandatory
        public string Alpha2Code { get; set; }
        public string Alpha3Code { get; set; }
        public List<string> CallingCodes { get; set; }
        public string Capital { get; set; }  //mandatory
        public List<string> altSpellings { get; set; }
        public string Region { get; set; }  //mandatory
        public string Subregion { get; set; }
        public int Population { get; set; }  //mandatory
        public List<double> Latlng { get; set; }
        public string demonym { get; set; }
        public double? Area { get; set; }
        public double? gini { get; set; }
        public List<string> Timezones { get; set; }
        public List<string> Borders { get; set; }
        public string NativeName { get; set; }
        public string numericCode { get; set; }
        public List<Currency> Currencies { get; set; }  //mandatory
        public List<Language> Languages { get; set; }
        public Translations Translations { get; set; }
        public string Flag { get; set; }  //mandatory
        public byte[] FlagBytes { get; set; } 
        public List<RegionalBloc> regionalBlocs { get; set; }
        public string cioc { get; set; }
        public string DateDBUpdade { get; set; }


        //public override string ToString()
        //{
        //    return $"{Name}";
        //}

        public string LabelLatlngTxt()
        {
            string Ll = "";
            if (Latlng.Count == 2)
            {

                Latlng[0] = Math.Round(Latlng[0]); Latlng[1] = Math.Round(Latlng[1]);
                Ll = Latlng[1] > 0 ? Latlng[1] + $"° E\n" : Latlng[1].ToString().Remove(0, 1) + $"° W\n";
                Ll += Latlng[0] > 0 ? Latlng[0] + $"° N" : Latlng[0].ToString().Remove(0, 1) + $"° S";
            }
            return Ll;
        }
    }
}
