﻿namespace Paises
{
    using Modelos;
    using Servicos;
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    public partial class Form1 : Form
    {

        bool load;
        bool tzCreated;
        Panel[] panelsTZ;
        Label[] labelsTZ;
        PictureBox[] pinBorders;
        Label[] labelBorders;
        const int originalSize = 700;



        //Country Pais = new Country();
        Country paisB = new Country();
        private List<Country> Paises;
        private NetworkService networkService;
        private ApiService apiService;
        private DialogService dialogService;
        private DataService dataService;
        private ImageService imageService;
        private MapService mapService;
        

        //ImageList imageList1 = new ImageList();


        public object AutocompleteSource { get; private set; }


        public Form1()
        {
            InitializeComponent();
            networkService = new NetworkService();
            apiService = new ApiService();
            dialogService = new DialogService();
            dataService = new DataService();
            imageService = new ImageService();
            mapService = new MapService();
            LoadPaises();

        }


        private void Form1_Load(object sender, EventArgs e)
        {
            Image img = Image.FromFile(@"..\..\Resources\mapaMundiSqrsMercator.png");
            Image img2 = Image.FromFile(@"..\..\Resources\mapaMundiSqrsMercatorTrq.png");

            panelMapa.BackgroundImage = imageService.ResizeImage(img, 700, 700);
            panelMapa.Size = new Size(panelMapa.BackgroundImage.Width, panelMapa.BackgroundImage.Height);

            BackgroundImage = imageService.ResizeImage(img2, 700, 700);
            ClientSize = new Size(Width, BackgroundImage.Height);

            comboBox1.Enabled = false;

            checkBoxFronteiras.Checked = true;
            checkBoxFusos.Checked = true;
            checkBoxFusos.Location = new Point(panelMapa.Right - checkBoxFusos.Width, panelMapa.Top + 5);
            checkBoxFronteiras.Location = new Point(checkBoxFusos.Left - checkBoxFronteiras.Width - 5, panelMapa.Top + 5);

            panelTZ.Width = panelMapa.Width / 25;





        }


        private void CarregaLista()
        {
            //ListView------------------------------------------------------------------

            listView1.Columns.Add("PAÍS");
            listView1.Columns.Add("ÁREA");
            listView1.Columns.Add("POPULAÇÃO");



            var lista = from Pais in Paises where Pais.Region == paisB.Region select Pais;

            foreach (var paiz in lista)
            {
                ListViewItem item;

                item = listView1.Items.Add(paiz.Translations.pt);
                item.SubItems.Add(paiz.Area.ToString());
                item.SubItems.Add(paiz.Population.ToString());

                //comboBox2.Items.Add(func.Nome);


            }

            listView1.Columns[0].Width = 140;
            for (int idx = 1; idx <= 2; idx++)
            {
                listView1.Columns[idx].AutoResize(ColumnHeaderAutoResizeStyle.HeaderSize);
            }


        }




        private async void LoadPaises()
        {
                       

            labelACarregar.Text = "A atualizar os países...";

            var connection = networkService.CheckConnection();

            if (!connection.IsSuccess)
            {
                LoadLocalPaises();
                load = false;
            }
            else
            {
                ControlsVisibility(false);

                await LoadApiPaises();
                load = true;
            }

                

            if (Paises.Count == 0)
            {
                labelACarregar.Text = "Não há ligação à Internet\n" +
                    "e não foram previamente carreagados os países" + Environment.NewLine +
                    "Tente mais tarde!";

                labelStatus.Text = "Primeira inicialização requer acesso à Internet";
                return;
            }


            PopulateCombo();
            comboBox1.Enabled = true;


            labelACarregar.Text = "Países atualizados";


            if (load)
            {
                labelStatus.Text = ($"Países carregados com sucesso em " +
                    DateTime.Now.ToString("dd MMM yyyy HH:mm: ss z"));
            }
            else
            {
                labelStatus.Text = ($"Países carregados da Base de Dados, atualizada em " +
                    File.GetLastWriteTime(@"Data\Paises.sqlite").ToString("dd MMM yyyy HH:mm: ss z"));
            }
        }



        private void LoadLocalPaises()
        {
            Paises = dataService.GetData();

        }


        private async Task LoadApiPaises()
        {
            var response = await apiService.GetPaises("https://restcountries.eu", "/rest/v2/all");

            Paises = (List<Country>)response.Result;

            progressBar1.Maximum = Paises.Count;

            //dataService.DeleteData();

            //dataService.SaveData(Paises, progressBar1, labelACarregar);

            
        }




        private void ControlsVisibility(bool vsbl)
        {
            if (vsbl)
            {

                panelInfo.Visible = true;
                labelPais.Visible = true;
                pictureBoxPin.Visible = true;
                labelLatlng.Visible = true;
                if (checkBoxFusos.Checked == true)
                {
                    labelTZ.Visible = true;
                    foreach (var item in panelsTZ.Zip(labelsTZ, (a, b) => new { A = a, B = b }))
                    {
                        item.A.Visible = true;
                        item.B.Visible = true;

                    }
                }
                checkBoxFronteiras.Visible = true;
                checkBoxFusos.Visible = true;
                if (checkBoxFronteiras.Checked == true)
                {
                    for (int i = 0; i < labelBorders.Count(); i++)
                    {
                        pinBorders[i].Visible = true;
                        labelBorders[i].Visible = true;
                    }
                    labelFronteiras.Visible = true;
                }

                progressBar1.Visible = false;
                labelACarregar.Visible = false;

                return;
            }

            labelFronteiras.Visible = false;
            if (labelBorders != null)
                for (int i = 0; i < labelBorders.Count(); i++)
                {
                    pinBorders[i].Visible = false;
                    labelBorders[i].Visible = false;
                }
            if (panelsTZ != null)
                foreach (var panel in panelsTZ)
                    panel.Visible = false;
            labelTZ.Visible = false;
            checkBoxFronteiras.Visible = false;
            checkBoxFusos.Visible = false;
            labelLatlng.Visible = false;
            pictureBoxPin.Visible = false;
            labelPais.Visible = false;
            panelInfo.Visible = false;

        }



        private void OrderControls()
        {
            labelTZ.BringToFront();
            if (panelsTZ != null)
                foreach (var panel in panelsTZ)
                    panel.BringToFront();
            labelUtc.BringToFront();
            pictureBoxPin.BringToFront();
            for (int i = 0; i < labelBorders.Count(); i++)
            {
                pinBorders[i].BringToFront();
                labelBorders[i].BringToFront();
            }
            panelInfo.BringToFront();
            labelPais.BringToFront();
            labelStatus.BringToFront();
            labelACarregar.BringToFront();
            comboBox1.BringToFront();
            checkBoxFusos.BringToFront();
            checkBoxFronteiras.BringToFront();
            foreach (var label in labelsTZ) //mapa
                label.BringToFront();
            labelLatlng.BringToFront(); //mapa
            labelFronteiras.BringToFront(); //mapa
            panelMapa.SendToBack();
            
        }



        public void PopulateCombo()
        {

            comboBox1.DisplayMember = "Name";
            //comboBox1.ValueMember = "value";
            comboBox1.AutoCompleteSource = AutoCompleteSource.ListItems;
            comboBox1.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            comboBox1.DataSource = Paises;

        }



        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

            paisB = (Country)comboBox1.SelectedItem;

            ControlsVisibility(false);

            LoadLabels();
 
            panelInfo.Height = labelMoeda.Bottom - pictureBox2.Top + 20;


            if (load)
            {
                if (!Directory.Exists("images"))
                     Directory.CreateDirectory("images");

                byte[] imageBytes = imageService.SvgToByteArray(paisB.Flag);

                MemoryStream ms = new MemoryStream(imageBytes);

                pictureBox1.BackgroundImage = Image.FromStream(ms, true);
                    
            }
            else
            {
                pictureBox1.BackgroundImage = imageService.byteArrayToSvg(
                    dataService.BlobReader(paisB.Alpha2Code));
            }

            //pictureBox1.BackgroundImage = flag;
            
            pictureBox1.BackgroundImageLayout = ImageLayout.Stretch;
            pictureBox1.BackColor = Color.Green;

            TravelingPin();


            //ListViewLoader();

            //var c = GetAll(this);
            //foreach (var b in c)
            //    //if (b.Name == "panelInfo")
            //        MessageBox.Show(b.Name + " +++++++ " + b.Parent.Name);

            //MessageBox.Show(ClientSize + "**" + panelMapa.Height);

            //MessageBox.Show((checkBoxFronteiras.Checked == true).ToString());

        }

        private void LoadLabels()
        {
            labelPais.Text = "País: " + paisB.Translations.pt.ToUpper() + $" ({paisB.Alpha2Code} - {paisB.Alpha3Code})";
            labelPais.Location = new Point(185 + (panelMapa.Width - 185) / 2 - labelPais.Width / 2, 50);
            labelPais.ForeColor = Color.Silver;
            labelNative.Text = paisB.NativeName;
            labelCapital.Text = "Capital: " + paisB.Capital;
            labelRegiao.Text = "Região: " + paisB.Region;
            labelArea.Text = "Área: " + paisB.Area;
            labelPopulacao.Text = "População: " + paisB.Population;
            labelDominio.Text = "Domínio Internet: " + paisB.TopLevelDomain[0];

            List<string> moedas = new List<string>();
            foreach (Currency crrncy in paisB.Currencies)
                moedas.Add(crrncy.Name + $" ({crrncy.Symbol})");
            labelMoeda.Text = "Moedas: " + string.Join(", ", moedas);
            List<string> linguas = new List<string>();
            foreach (Language lngge in paisB.Languages)
                linguas.Add(lngge.Name);
            labelMoeda.Text += "\n\nLínguas: " + string.Join(", ", linguas);
            //labelLingua.Location = new Point(labelPais.Right,
            //    labelPais.Bottom - labelLingua.Height);
            labelMoeda.Left = panelInfo.Width / 2 - labelMoeda.Width / 2;
        }

        private void TravelingPin()
        {

            // são realizadas operações relacionadas com o mapa => sinalização

            panelInfo.BackColor = Color.FromArgb(200, Color.DarkTurquoise);  // painel com bandeira e info
            pictureBoxPin.BackColor = Color.FromArgb(200, Color.DarkTurquoise);  // 'pin' que sinaliza país
            panelTZ.BackColor = Color.FromArgb(50, Color.White);  // painel que sinaliza os fusos (as time zones)



            CreateTZPanels();

            tzCreated = CreateTZLabels(tzCreated);

            InsertPin(pictureBoxPin, paisB.Latlng, labelLatlng, paisB.LabelLatlngTxt());

            InsertBorderPins();

            OrderControls();

            ControlsVisibility(true);

            CarregaLista();



        }



        private void InsertBorderPins()
        {
            // criados e inseridos mini-pins dos 'países-fronteira' + respctvs. labels

            // elimina os pins dos paises que fazem fronteira, se houver
            if (pinBorders != null)
                for (int i = 0; i < pinBorders.Length; i++)  
                {
                    //Controls.Remove(pinBorders[i]);
                    //Controls.Remove(labelBorders[i]);
                    pinBorders[i].Dispose();
                    labelBorders[i].Dispose();
                }

            // lista de paises que fazem fronteira
            var pesquisa = from Pais in Paises
                           where paisB.Borders.Contains(Pais.Alpha3Code)  
                           select Pais;

            //MessageBox.Show(pesquisa.Count().ToString());

            labelBorders = new Label[pesquisa.Count()];
            pinBorders = new PictureBox[pesquisa.Count()];

            List<string> paisesBord = new List<string>();

            double maxX = 0.0;
            double minX = 700;
            Point auxX;
            int n = 0;
            foreach (var pb in pesquisa)
            {

                pinBorders[n] = new PictureBox
                {
                    BackColor = Color.Red,
                    Size = new Size(5, 5),
                };
                Controls.Add(pinBorders[n]);
                pinBorders[n].Visible = false;

                labelBorders[n] = new Label
                {
                    BackColor = Color.Black,
                    TextAlign = ContentAlignment.MiddleCenter,
                    Font = new Font("MS Sans Serif", 6, FontStyle.Bold),
                    ForeColor = Color.Red,
                    AutoSize = true,
                };
                Controls.Add(labelBorders[n]);
                labelBorders[n].Visible = false;

                InsertPin(pinBorders[n], pb.Latlng, labelBorders[n], pb.Alpha3Code);
                n++;

                paisesBord.Add(pb.Translations.pt.Length < 12 ? pb.Translations.pt : pb.Translations.pt.Substring(0, 12) + "...");

                auxX = mapService.CoordsToMapa(pb.Latlng[1], pb.Latlng[0], panelMapa.Width, Height, pictureBoxPin);
                if (auxX.X > maxX)
                    maxX = auxX.X;

                if (auxX.X < minX)
                    minX = auxX.X;
            }

            //MessageBox.Show(maxX.ToString(), minX.ToString());

            insertLabelFronteiras(paisesBord, maxX, minX);

        }




        private void insertLabelFronteiras(List<string> paisesFronteira, double maxX, double minX)
        {

            if (paisesFronteira.Count() == 0)
            {
                labelFronteiras.Text = "";
                return;
            }

            labelFronteiras.BackColor = Color.Transparent;
            labelFronteiras.Parent = panelMapa;
            //labelFronteiras.Font = new Font(labelFronteiras.Font, labelFronteiras.Font.Style ^ FontStyle.Italic);
            labelFronteiras.Font = new Font("MS Sans Serif", 8, FontStyle.Bold);
            labelFronteiras.ForeColor = Color.Red;

            labelFronteiras.Text = "FRONTEIRAS:\n\n\u25A0  ";
            labelFronteiras.Text += string.Join("\n\u25A0  ", paisesFronteira);

            if (pictureBoxPin.Left - pictureBoxPin.Width / 2 > panelMapa.Width / 2)
            {
                if (panelInfo.Right > minX)
                {
                    panelInfo.Left = (int)minX - panelInfo.Width - 30;
                }
            }
            else
            {
                if (panelInfo.Left < maxX)
                    panelInfo.Left = (int)maxX + 30;
            }


            //labelFronteiras.Location = labelFronteiras.Left < 0 ? new Point(0, labelPais.Bottom + 30) :
            //    panelInfo.Location;

            //if (labelFronteiras.Right > panelMapa.Width)
            //{
            //    labelFronteiras.BackColor = Color.Black;
            //    labelFronteiras.Parent = this;
            //}


            foreach (var tz in panelsTZ)
                if (labelFronteiras.Right > tz.Left && labelFronteiras.Left < tz.Right)
                {
                    labelFronteiras.BackColor = Color.Black;
                    labelFronteiras.Parent = this;
                    //labelFronteiras.BringToFront();
                }

            labelFronteiras.Location = new Point(20 * panelMapa.Height / 700 , 400);

        }



        private void InsertPin(PictureBox pin, List<double> ltlg, Label labelX, string txt)
        {
            // posiciona o 'pin' que sinaliza o país + Label

            //pictureBoxPin.SizeMode = PictureBoxSizeMode.AutoSize;
            Point ponto = new Point();

            if (ltlg.Count == 2)
                ponto = mapService.CoordsToMapa(ltlg[1], ltlg[0], panelMapa.Width, Height, pictureBoxPin);
            int x = ponto.X;
            int y = ponto.Y;

            //Bitmap bitmap1;
            //bitmap1 = (Bitmap)Image.FromFile(@"..\..\Resources\pinDotConeUp2.png");

            pin.Location = new Point(x - pin.Width / 2, y - pin.Height / 2);

            //bool downUnder = false;
            int defaultY = 150;
            panelInfo.Top = defaultY;
            
            labelX.Text = txt;
            
            int maxPTZ = 0, minPTZ = 700;
            foreach (var ptz in panelsTZ)
            {
                if (maxPTZ < ptz.Right)
                    maxPTZ = ptz.Right;
                if (minPTZ > ptz.Left)
                    minPTZ = ptz.Left;
            }
            //MessageBox.Show(maxPTZ + "  " + minPTZ);

            if (pin == pictureBoxPin)
            {
                labelX.Parent = panelMapa;
                labelX.BackColor = Color.Transparent;

                if (pictureBoxPin.Location.X - pictureBoxPin.Width / 2 > panelMapa.Width / 2)
                {
                    panelInfo.Location = new Point(pin.Location.X - panelInfo.Width - 50, defaultY);
                    labelX.Location = new Point(pin.Right + 10, pin.Top - (int)(1.5 * labelX.Height));
                    labelX.Left = labelX.Right > panelMapa.Width ? panelMapa.Width - labelX.Width :
                        labelX.Left < maxPTZ ? maxPTZ : labelX.Left;
                }
                else
                {
                    panelInfo.Location = new Point(pin.Location.X + pin.Width + 50, defaultY);
                    labelX.Location = new Point(pin.Left - labelX.Width - 10, pin.Top - (int)(1.5 * labelX.Height));
                    labelX.Left = labelX.Left < 0 ? 0 : labelX.Right > minPTZ && labelX.Left < minPTZ + 28 ?
                        minPTZ - labelX.Width : labelX.Left;
                }

                foreach (var tz in panelsTZ)
                    if (labelX.Right > tz.Left && labelX.Left < tz.Right)
                    {
                        labelX.BackColor = Color.Black;
                        labelX.Parent = this;
                        //labelX.BringToFront();
                    }

                if(labelX.Right > panelMapa.Width)
                {
                    labelX.BackColor = Color.Black;
                    labelX.Parent = this;
                }
            }

            else
            {
                if (pin.Left + pin.Width / 2 > pictureBoxPin.Left + pictureBoxPin.Width / 2)   
                    labelX.Location = new Point(pin.Right, pin.Top);
                else
                    labelX.Location = new Point(pin.Left - labelX.Width, pin.Top);
            }        


        }



        private bool CreateTZLabels(bool created)
        {

            if (!created)
            {
                // Cria as Labels com as referências (marcadores) das TZ (time zones)
                // na parte inferior do mapa...

                int n = 25;
                Label[] labels = new Label[n];
                for (int i = 0; i < n; i++)
                {
                    labels[i] = new Label
                    {
                        BackColor = Color.Transparent,
                        Size = new Size(panelMapa.Width / 25, 25),
                        Top = panelMapa.Height - 25,
                        TextAlign = ContentAlignment.MiddleCenter,
                        Font = new Font("MS Sans Serif", 8, FontStyle.Bold),
                        ForeColor = Color.DarkGray,
                        AutoSize = false,
                    };

                    labels[i].BorderStyle = BorderStyle.FixedSingle;
                    //labels[i].BringToFront();

                }

                int y = -12;
                for (int i = 0; i < n; i++)
                {
                    labels[i].Left = mapService.timeZoneLocX2(y, panelMapa.Width);
                    labels[i].Text = Convert.ToString(y);
                    panelMapa.Controls.Add(labels[i]);
                    y++;
                }

                labelTZ.Text = string.Join("\n", paisB.Timezones);
                labelTZ.Left = panelsTZ[0].Left - labelTZ.Width - 20;
                labelTZ.Left = labelTZ.Left < 0 ? panelsTZ[panelsTZ.Length - 1].Right + 20 : labelTZ.Left;
                labelTZ.Top = panelMapa.Height - 35 - labelTZ.Height;
                labelTZ.ForeColor = Color.LightGray;

                return true;
            }

            //labelTZ.Text = "Fusos Horários:\n\n";
            labelTZ.Text = string.Join("\n", paisB.Timezones);
            labelTZ.Left = panelsTZ[0].Left - labelTZ.Width - 20;
            labelTZ.Left = labelTZ.Left < 0 ? panelsTZ[panelsTZ.Length - 1].Right + 20 : labelTZ.Left;
            labelTZ.Top = panelMapa.Height - 35 - labelTZ.Height;


            return true;
        }



        private void CreateTZPanels()
        {

            // elimina e cria ('clona' - através do método Clone) novos paineis de 
            // time zone (c/ as respectivas labels c/ indicação da TZ), para cada país, em arrays

            // elimina as time zones (TZ) que lá estão
            if (panelsTZ != null)
                for (int i = 0; i < panelsTZ.Length; i++)
                {
                    //Controls.Remove(panelsTZ[i]);
                    panelsTZ[i].Dispose();
                }


            // instancia novas
            panelsTZ = new Panel[paisB.Timezones.Count];
            labelsTZ = new Label[paisB.Timezones.Count];   

            for (int i = 0; i < paisB.Timezones.Count; i++)
            {
                // clona 1 p/ cada TZ (c/ propriedades)
                panelsTZ[i] = new Panel();
                panelsTZ[i] = panelTZ.Clone();  


                labelsTZ[i] = new Label();
                labelsTZ[i] = labelUtc.Clone();

                // adiciona a label ao painel TZ
                panelsTZ[i].Controls.Add(labelsTZ[i]);  

                labelUtc.Text = "";

                // vai buscar coverte a(s) TZ para coordenadas do mapa
                if (paisB.Timezones[i] == "UTC")
                    panelsTZ[i].Left = mapService.timeZoneLocX("UTC 00:00", panelMapa.Width) + 19;   
                else
                    panelsTZ[i].Left = mapService.timeZoneLocX(paisB.Timezones[i], panelMapa.Width) + 19;

                // Adiciona a ref. da TZ à label a 1ª e a última, if > 1 TZ (voltei à 1ª forma)
                //if (i == 0 || i == panelsTZ.Length - 1)  
                labelsTZ[i].Text = paisB.Timezones[i].Length == 1 ? "NA" :  
                        paisB.Timezones[i].Length == 3 ? "0" :
                        paisB.Timezones[i].Length == 6 ? paisB.Timezones[i].Substring(3, 3) :
                        paisB.Timezones[i][7] == '0' ?
                        paisB.Timezones[i].Substring(3, 3) : paisB.Timezones[i].Substring(3, 3) + "\n.30";

                // adiciona os paineis TZ com as labels ao painel Mapa
                Controls.Add(panelsTZ[i]);
                //panelsTZ[i].Parent = this;
                //labelsTZ[i].Visible = true;
                //panelsTZ[i].Visible = true;
                //panelsTZ[i].BringToFront();
                //labelPais.BringToFront();
                //pictureBoxPin.BringToFront();



            }
        }

        private void checkBoxFronteiras_CheckedChanged(object sender, EventArgs e)
        {
            labelFronteiras.Visible = labelFronteiras.Visible == false ? true : false;
            if (labelBorders != null)
                for (int i = 0; i < labelBorders.Count(); i++)
                {
                    pinBorders[i].Visible = pinBorders[i].Visible == false ? true : false;
                    labelBorders[i].Visible = labelBorders[i].Visible == false ? true : false;
                }
        }



        private void checkBoxFusos_CheckedChanged(object sender, EventArgs e)
        {
            labelTZ.Visible = labelTZ.Visible == false ? true : false;
            if (labelsTZ != null)
                for (int i = 0; i < labelsTZ.Count(); i++)
                {
                    labelsTZ[i].Visible = labelsTZ[i].Visible == false ? true : false;
                    panelsTZ[i].Visible = panelsTZ[i].Visible == false ? true : false;
                }
        }



        public IEnumerable<Control> GetAll(Control control) //, Type type)
        {
            var controls = control.Controls.Cast<Control>();


            return controls.SelectMany(ctrl => GetAll(ctrl)) //, type))
                                      .Concat(controls);

                                      //.Where(c => c.GetType() == type);
        }




        //private void panel1_MouseMove(object sender, MouseEventArgs e)
        //{
        //    Point ptE = new Point(e.Location.X, e.Location.Y);
        //    ptE = PointToClient(ptE);
        //    double lng = mapService.MapaToCoords(ptE.X, ptE.Y,Width, Height, pictureBoxPin)[0];
        //    double lat = mapService.MapaToCoords(ptE.X, ptE.Y,panelMapa.Width, Height, pictureBoxPin)[1];
        //    //Text = e.Location.X + ":" + e.Location.Y;
        //    Text = string.Format("Paises   lng: {0:N2} lat: {1:N2}", lng, lat);
        //}


    }
}
