﻿namespace Paises
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param Name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.panelInfo = new System.Windows.Forms.Panel();
            this.labelMoeda = new System.Windows.Forms.Label();
            this.labelDominio = new System.Windows.Forms.Label();
            this.labelPopulacao = new System.Windows.Forms.Label();
            this.labelArea = new System.Windows.Forms.Label();
            this.labelRegiao = new System.Windows.Forms.Label();
            this.labelCapital = new System.Windows.Forms.Label();
            this.labelNative = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.labelPais = new System.Windows.Forms.Label();
            this.labelStatus = new System.Windows.Forms.Label();
            this.labelTZ = new System.Windows.Forms.Label();
            this.panelTZ = new System.Windows.Forms.Panel();
            this.labelUtc = new System.Windows.Forms.Label();
            this.labelLatlng = new System.Windows.Forms.Label();
            this.labelACarregar = new System.Windows.Forms.Label();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.pictureBoxPin = new System.Windows.Forms.PictureBox();
            this.panelMapa = new System.Windows.Forms.Panel();
            this.labelFronteiras = new System.Windows.Forms.Label();
            this.pictureBoxBorder = new System.Windows.Forms.PictureBox();
            this.labelBorder = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.checkBoxFronteiras = new System.Windows.Forms.CheckBox();
            this.checkBoxFusos = new System.Windows.Forms.CheckBox();
            this.panelInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panelTZ.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBorder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBox1
            // 
            this.comboBox1.BackColor = System.Drawing.Color.Black;
            this.comboBox1.DropDownWidth = 200;
            this.comboBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1.ForeColor = System.Drawing.Color.White;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(16, 21);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(183, 23);
            this.comboBox1.TabIndex = 1;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // progressBar1
            // 
            this.progressBar1.BackColor = System.Drawing.Color.Black;
            this.progressBar1.ForeColor = System.Drawing.Color.MediumTurquoise;
            this.progressBar1.Location = new System.Drawing.Point(279, 558);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(152, 21);
            this.progressBar1.Step = 1;
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar1.TabIndex = 5;
            // 
            // panelInfo
            // 
            this.panelInfo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelInfo.Controls.Add(this.labelMoeda);
            this.panelInfo.Controls.Add(this.labelDominio);
            this.panelInfo.Controls.Add(this.labelPopulacao);
            this.panelInfo.Controls.Add(this.labelArea);
            this.panelInfo.Controls.Add(this.labelRegiao);
            this.panelInfo.Controls.Add(this.labelCapital);
            this.panelInfo.Controls.Add(this.labelNative);
            this.panelInfo.Controls.Add(this.pictureBox1);
            this.panelInfo.Controls.Add(this.pictureBox2);
            this.panelInfo.Location = new System.Drawing.Point(717, 23);
            this.panelInfo.Name = "panelInfo";
            this.panelInfo.Size = new System.Drawing.Size(185, 300);
            this.panelInfo.TabIndex = 6;
            // 
            // labelMoeda
            // 
            this.labelMoeda.AutoSize = true;
            this.labelMoeda.BackColor = System.Drawing.Color.Transparent;
            this.labelMoeda.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMoeda.ForeColor = System.Drawing.Color.Black;
            this.labelMoeda.Location = new System.Drawing.Point(0, 221);
            this.labelMoeda.MaximumSize = new System.Drawing.Size(185, 0);
            this.labelMoeda.Name = "labelMoeda";
            this.labelMoeda.Size = new System.Drawing.Size(0, 13);
            this.labelMoeda.TabIndex = 11;
            this.labelMoeda.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelDominio
            // 
            this.labelDominio.BackColor = System.Drawing.Color.Transparent;
            this.labelDominio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDominio.ForeColor = System.Drawing.Color.Black;
            this.labelDominio.Location = new System.Drawing.Point(1, 199);
            this.labelDominio.Name = "labelDominio";
            this.labelDominio.Size = new System.Drawing.Size(185, 25);
            this.labelDominio.TabIndex = 12;
            this.labelDominio.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelPopulacao
            // 
            this.labelPopulacao.BackColor = System.Drawing.Color.Transparent;
            this.labelPopulacao.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPopulacao.ForeColor = System.Drawing.Color.Black;
            this.labelPopulacao.Location = new System.Drawing.Point(0, 178);
            this.labelPopulacao.Name = "labelPopulacao";
            this.labelPopulacao.Size = new System.Drawing.Size(185, 25);
            this.labelPopulacao.TabIndex = 10;
            this.labelPopulacao.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelArea
            // 
            this.labelArea.BackColor = System.Drawing.Color.Transparent;
            this.labelArea.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelArea.ForeColor = System.Drawing.Color.Black;
            this.labelArea.Location = new System.Drawing.Point(0, 156);
            this.labelArea.Name = "labelArea";
            this.labelArea.Size = new System.Drawing.Size(185, 25);
            this.labelArea.TabIndex = 16;
            this.labelArea.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelRegiao
            // 
            this.labelRegiao.BackColor = System.Drawing.Color.Transparent;
            this.labelRegiao.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRegiao.ForeColor = System.Drawing.Color.Black;
            this.labelRegiao.Location = new System.Drawing.Point(0, 134);
            this.labelRegiao.Name = "labelRegiao";
            this.labelRegiao.Size = new System.Drawing.Size(185, 25);
            this.labelRegiao.TabIndex = 9;
            this.labelRegiao.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelCapital
            // 
            this.labelCapital.BackColor = System.Drawing.Color.Transparent;
            this.labelCapital.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCapital.ForeColor = System.Drawing.Color.Black;
            this.labelCapital.Location = new System.Drawing.Point(0, 113);
            this.labelCapital.Name = "labelCapital";
            this.labelCapital.Size = new System.Drawing.Size(185, 25);
            this.labelCapital.TabIndex = 8;
            this.labelCapital.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // labelNative
            // 
            this.labelNative.BackColor = System.Drawing.Color.Transparent;
            this.labelNative.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNative.ForeColor = System.Drawing.Color.White;
            this.labelNative.Location = new System.Drawing.Point(0, 89);
            this.labelNative.Name = "labelNative";
            this.labelNative.Size = new System.Drawing.Size(185, 17);
            this.labelNative.TabIndex = 15;
            this.labelNative.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(44, 10);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(98, 70);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.White;
            this.pictureBox2.Location = new System.Drawing.Point(42, 8);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(102, 74);
            this.pictureBox2.TabIndex = 14;
            this.pictureBox2.TabStop = false;
            // 
            // labelPais
            // 
            this.labelPais.AutoSize = true;
            this.labelPais.BackColor = System.Drawing.Color.Transparent;
            this.labelPais.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPais.ForeColor = System.Drawing.Color.LightGray;
            this.labelPais.Location = new System.Drawing.Point(718, 558);
            this.labelPais.Name = "labelPais";
            this.labelPais.Size = new System.Drawing.Size(0, 18);
            this.labelPais.TabIndex = 7;
            this.labelPais.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelStatus
            // 
            this.labelStatus.BackColor = System.Drawing.Color.Transparent;
            this.labelStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStatus.ForeColor = System.Drawing.Color.DarkGray;
            this.labelStatus.Location = new System.Drawing.Point(0, 0);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Padding = new System.Windows.Forms.Padding(10, 0, 3, 0);
            this.labelStatus.Size = new System.Drawing.Size(1035, 21);
            this.labelStatus.TabIndex = 9;
            // 
            // labelTZ
            // 
            this.labelTZ.AutoSize = true;
            this.labelTZ.BackColor = System.Drawing.Color.Transparent;
            this.labelTZ.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTZ.ForeColor = System.Drawing.Color.White;
            this.labelTZ.Location = new System.Drawing.Point(276, 615);
            this.labelTZ.Name = "labelTZ";
            this.labelTZ.Size = new System.Drawing.Size(0, 13);
            this.labelTZ.TabIndex = 10;
            // 
            // panelTZ
            // 
            this.panelTZ.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panelTZ.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelTZ.Controls.Add(this.labelUtc);
            this.panelTZ.Location = new System.Drawing.Point(456, 0);
            this.panelTZ.Name = "panelTZ";
            this.panelTZ.Size = new System.Drawing.Size(28, 700);
            this.panelTZ.TabIndex = 11;
            this.panelTZ.Visible = false;
            // 
            // labelUtc
            // 
            this.labelUtc.BackColor = System.Drawing.Color.Transparent;
            this.labelUtc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUtc.ForeColor = System.Drawing.Color.DarkTurquoise;
            this.labelUtc.Location = new System.Drawing.Point(-7, 660);
            this.labelUtc.Margin = new System.Windows.Forms.Padding(0);
            this.labelUtc.Name = "labelUtc";
            this.labelUtc.Size = new System.Drawing.Size(40, 35);
            this.labelUtc.TabIndex = 0;
            this.labelUtc.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelLatlng
            // 
            this.labelLatlng.AutoSize = true;
            this.labelLatlng.BackColor = System.Drawing.Color.Transparent;
            this.labelLatlng.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLatlng.ForeColor = System.Drawing.Color.Turquoise;
            this.labelLatlng.Location = new System.Drawing.Point(743, 600);
            this.labelLatlng.Name = "labelLatlng";
            this.labelLatlng.Size = new System.Drawing.Size(0, 15);
            this.labelLatlng.TabIndex = 12;
            // 
            // labelACarregar
            // 
            this.labelACarregar.BackColor = System.Drawing.Color.Transparent;
            this.labelACarregar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelACarregar.ForeColor = System.Drawing.Color.DarkGray;
            this.labelACarregar.Location = new System.Drawing.Point(276, 584);
            this.labelACarregar.Name = "labelACarregar";
            this.labelACarregar.Size = new System.Drawing.Size(276, 23);
            this.labelACarregar.TabIndex = 17;
            this.labelACarregar.Text = "Resultado...";
            // 
            // treeView1
            // 
            this.treeView1.BackColor = System.Drawing.Color.Black;
            this.treeView1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.treeView1.Location = new System.Drawing.Point(898, 592);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(109, 96);
            this.treeView1.TabIndex = 19;
            // 
            // pictureBoxPin
            // 
            this.pictureBoxPin.BackColor = System.Drawing.Color.Transparent;
            this.pictureBoxPin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxPin.Location = new System.Drawing.Point(721, 668);
            this.pictureBoxPin.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBoxPin.Name = "pictureBoxPin";
            this.pictureBoxPin.Size = new System.Drawing.Size(22, 22);
            this.pictureBoxPin.TabIndex = 7;
            this.pictureBoxPin.TabStop = false;
            // 
            // panelMapa
            // 
            this.panelMapa.BackgroundImage = global::Paises.Properties.Resources.mapaMundiSqrsMercator;
            this.panelMapa.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panelMapa.Location = new System.Drawing.Point(0, 0);
            this.panelMapa.Margin = new System.Windows.Forms.Padding(0);
            this.panelMapa.Name = "panelMapa";
            this.panelMapa.Size = new System.Drawing.Size(700, 700);
            this.panelMapa.TabIndex = 18;
            // 
            // labelFronteiras
            // 
            this.labelFronteiras.AutoSize = true;
            this.labelFronteiras.BackColor = System.Drawing.Color.Transparent;
            this.labelFronteiras.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFronteiras.ForeColor = System.Drawing.Color.Red;
            this.labelFronteiras.Location = new System.Drawing.Point(718, 602);
            this.labelFronteiras.Name = "labelFronteiras";
            this.labelFronteiras.Size = new System.Drawing.Size(0, 15);
            this.labelFronteiras.TabIndex = 22;
            // 
            // pictureBoxBorder
            // 
            this.pictureBoxBorder.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBoxBorder.Location = new System.Drawing.Point(721, 636);
            this.pictureBoxBorder.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBoxBorder.Name = "pictureBoxBorder";
            this.pictureBoxBorder.Size = new System.Drawing.Size(5, 5);
            this.pictureBoxBorder.TabIndex = 20;
            this.pictureBoxBorder.TabStop = false;
            // 
            // labelBorder
            // 
            this.labelBorder.AutoSize = true;
            this.labelBorder.BackColor = System.Drawing.Color.Black;
            this.labelBorder.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBorder.ForeColor = System.Drawing.Color.Red;
            this.labelBorder.Location = new System.Drawing.Point(732, 637);
            this.labelBorder.Name = "labelBorder";
            this.labelBorder.Size = new System.Drawing.Size(0, 9);
            this.labelBorder.TabIndex = 21;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImage = global::Paises.Properties.Resources.if_construction_basic_red_69863;
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox3.Location = new System.Drawing.Point(936, 625);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(30, 30);
            this.pictureBox3.TabIndex = 23;
            this.pictureBox3.TabStop = false;
            // 
            // checkBoxFronteiras
            // 
            this.checkBoxFronteiras.AutoSize = true;
            this.checkBoxFronteiras.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxFronteiras.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxFronteiras.ForeColor = System.Drawing.Color.Silver;
            this.checkBoxFronteiras.Location = new System.Drawing.Point(777, 652);
            this.checkBoxFronteiras.Name = "checkBoxFronteiras";
            this.checkBoxFronteiras.Size = new System.Drawing.Size(82, 17);
            this.checkBoxFronteiras.TabIndex = 24;
            this.checkBoxFronteiras.Text = "Fronteiras";
            this.checkBoxFronteiras.UseVisualStyleBackColor = false;
            this.checkBoxFronteiras.CheckedChanged += new System.EventHandler(this.checkBoxFronteiras_CheckedChanged);
            // 
            // checkBoxFusos
            // 
            this.checkBoxFusos.AutoSize = true;
            this.checkBoxFusos.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxFusos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxFusos.ForeColor = System.Drawing.Color.Silver;
            this.checkBoxFusos.Location = new System.Drawing.Point(777, 675);
            this.checkBoxFusos.Name = "checkBoxFusos";
            this.checkBoxFusos.Size = new System.Drawing.Size(110, 17);
            this.checkBoxFusos.TabIndex = 25;
            this.checkBoxFusos.Text = "Fusos Horários";
            this.checkBoxFusos.UseVisualStyleBackColor = false;
            this.checkBoxFusos.CheckedChanged += new System.EventHandler(this.checkBoxFusos_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(1009, 700);
            this.Controls.Add(this.checkBoxFusos);
            this.Controls.Add(this.checkBoxFronteiras);
            this.Controls.Add(this.panelInfo);
            this.Controls.Add(this.labelLatlng);
            this.Controls.Add(this.labelFronteiras);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.labelStatus);
            this.Controls.Add(this.labelBorder);
            this.Controls.Add(this.pictureBoxBorder);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.labelPais);
            this.Controls.Add(this.labelTZ);
            this.Controls.Add(this.pictureBoxPin);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.panelTZ);
            this.Controls.Add(this.labelACarregar);
            this.Controls.Add(this.panelMapa);
            this.DoubleBuffered = true;
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panelInfo.ResumeLayout(false);
            this.panelInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panelTZ.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBorder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Panel panelInfo;
        private System.Windows.Forms.Label labelPais;
        private System.Windows.Forms.Label labelCapital;
        private System.Windows.Forms.Label labelRegiao;
        private System.Windows.Forms.Label labelPopulacao;
        private System.Windows.Forms.Label labelMoeda;
        private System.Windows.Forms.Label labelDominio;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBoxPin;
        private System.Windows.Forms.Label labelNative;
        private System.Windows.Forms.Label labelStatus;
        private System.Windows.Forms.Label labelTZ;
        private System.Windows.Forms.Panel panelTZ;
        private System.Windows.Forms.Label labelLatlng;
        private System.Windows.Forms.Label labelUtc;
        private System.Windows.Forms.Label labelACarregar;
        private System.Windows.Forms.Panel panelMapa;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.PictureBox pictureBoxBorder;
        private System.Windows.Forms.Label labelBorder;
        private System.Windows.Forms.Label labelFronteiras;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label labelArea;
        private System.Windows.Forms.CheckBox checkBoxFronteiras;
        private System.Windows.Forms.CheckBox checkBoxFusos;
    }
}

